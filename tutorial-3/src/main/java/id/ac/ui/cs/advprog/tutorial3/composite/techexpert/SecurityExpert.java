package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees{

	public SecurityExpert(String name, double salary) {
		// TODO Auto-generated constructor stub
		if(salary<70000.00){
			throw new IllegalArgumentException();
		}
		
		else{
			this.name = name;
			this.salary = salary;
			this.role = "Security Expert";
		}
	}

	@Override
	public double getSalary() {
		// TODO Auto-generated method stub
		return this.salary;
	}

}
