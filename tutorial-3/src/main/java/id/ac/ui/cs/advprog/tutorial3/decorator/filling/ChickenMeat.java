package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Filling {
	Food food;
	
	public ChickenMeat(Food food) {
		// TODO Auto-generated constructor stub
		this.food = food;
	}
	
	@Override
    public String getDescription() {
        //TODO Implement
		return food.getDescription()+", adding chicken meat";
    }

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return food.cost() + 4.5;
	}

}
