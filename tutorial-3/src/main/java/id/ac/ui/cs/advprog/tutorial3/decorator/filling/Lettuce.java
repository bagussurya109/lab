package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Filling {
	Food food;

	public Lettuce(Food food) {
		// TODO Auto-generated constructor stub
		this.food = food;
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return food.cost() + 0.75;
	}

	@Override
	public String getDescription() {
		return food.getDescription()+", adding lettuce";
	}

}
