package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Filling {
	Food food;

	public TomatoSauce(Food food) {
		// TODO Auto-generated constructor stub
		this.food = food;
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return food.cost() + 0.2;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return food.getDescription()+", adding tomato sauce";
	}

}
