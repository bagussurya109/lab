package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.BleuCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SteamClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.BlackThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BarbequeSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Jalapeno;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory{

    public Dough createDough() {
        return new BlackThickCrustDough();
    }

    public Sauce createSauce() {
        return new BarbequeSauce();
    }

    public Cheese createCheese() {
        return new BleuCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Jalapeno(), new Spinach(), new Eggplant(), new BlackOlives()};
        return veggies;
    }

    public Clams createClam() {
        return new SteamClams();
    }
}
