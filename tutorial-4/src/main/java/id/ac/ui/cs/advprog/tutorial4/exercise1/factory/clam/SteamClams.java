package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SteamClams implements Clams {

    public String toString() {
        return "Steamed Clams from The Local Fish Market";
    }
}
