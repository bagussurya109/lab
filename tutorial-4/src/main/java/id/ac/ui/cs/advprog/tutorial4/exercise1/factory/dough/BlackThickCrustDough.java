package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class BlackThickCrustDough implements Dough {
    public String toString() {
        return "Black Thick Crust style extra thick crust black dough";
    }
}
